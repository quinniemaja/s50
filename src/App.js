// import {Fragment} from 'react'
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import Courses from './pages/Courses'
import CourseView from './pages/CourseView'
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'

import './App.css';
import {UserProvider} from './UserContext';

function App() {

  // 
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }


  
  useEffect(() => {
      console.log(user)
      console.log(localStorage)
  }, [user])

  /*
  the userprovider component is what allows other components to consume or use our context. any component which is not wrapped by userprovider wil not have access to the values provided for our context

  you can pass data or information to our context by providing a "value " attribute in our userprovider. data passed here by ither components by unwrapping our contezt using usecontext hook. 

  */

  return (
  <UserProvider value = {{user, setUser, unsetUser}}>
     <Router>
     	<AppNavbar/>
     	<Container>
     		<Routes>
     			<Route  path = "/" element = {<Home/>}/>
     			<Route  path = "/courses" element = {<Courses/>}/>
          <Route  path = "/courses/:courseId" element = {<CourseView/>}/>
     			<Route  path = "/login" element = {<Login/>}/>
     			<Route  path = "/register" element = {<Register/>}/>
     			<Route  path = "/logout" element = {<Logout/>}/>
          <Route  path = "/" element = {<Logout/>}/>
          <Route  path = "*" element = {<ErrorPage/>}/>
     		</Routes>
     	</Container>
   </Router>
  </UserProvider>
  );
}

export default App;


/*
	ReactJS is a single page application (SPA). However , we can simulate the changing of pages. We don't actually create new pages, what we just do is switch pages to their assigned routes. ReactJS and react-router-dom package mimics or mirrors how HTML access its URL. 


	Rreact-router-dom 
		3 main components to simulate the switching of pages.
			1. Router - wrapping the router components around other components will allow us to use routing within our page.
			2. Switch - allow us to switch change our page ocmponents
			3. Route - assigns paths which will trigger the change/switch of compnents render.
*/