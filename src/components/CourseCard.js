import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CourseCard ({courseProp}) {

	console.log(courseProp)

	// object deconstruction
	const {name, description, price, _id} = courseProp

	// array deconstruction



	

	return (

	
		<Card className = "cardHighlight p-3">
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className= 'btn btn-primary btn-block' to = {`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
			
		
	)

	
}