import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights () {

	return (

		<Row className = "my-3">
			<Col xs = {12} md = {4}> {/*if media query no need for className*/}
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Excepturi consequuntur rem fuga quia? Earum dolor, laborum, molestias consequuntur aut sed eligendi cumque pariatur ipsam. Labore ad, ullam quam perferendis neque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Excepturi consequuntur rem fuga quia? Earum dolor, laborum, molestias consequuntur aut sed eligendi cumque pariatur ipsam. Labore ad, ullam quam perferendis neque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Excepturi consequuntur rem fuga quia? Earum dolor, laborum, molestias consequuntur aut sed eligendi cumque pariatur ipsam. Labore ad, ullam quam perferendis neque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>


	)
}