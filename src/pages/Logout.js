import {useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext';


export default function Logout () {

	// localStorage.clear();

	// Consume the  Usercontext object and destructure it to access user state and unsetUser function from the context provider.

	const {unsetUser, setUser} = useContext(UserContext)
	unsetUser(); //to clear the local storage

	useEffect(() => {
		setUser({id:null});

	})

	return (

		<Navigate to= "/" />
	)
}