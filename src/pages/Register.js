import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register () {

	const { user} = useContext(UserContext);
	const history = useNavigate();

	//state hooks to store values of the input fields
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')

	//state to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)
	
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e){

		//prevents page Navigateion via form submission
		e.preventDefault();

		fetch('http://localHost:4000/user/register', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: 'Registered Successfully',
					icon: 'success',
					text: 'You have successfully Registered.'
				})

				history("/login");
			} else {
				Swal.fire({
					title: 'Oops! ',
					icon: 'error',
					text: 'Email Already Exist!'
				})
			}
		})

		// localStorage.setItem('email', email);
		// console.log(email)

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail(''); //to clear input field after submission
		setPassword1('');
		setPassword2('');

		// alert('You have successfully registered!')
	}

	 // Validation to enable submit button when all fields are populated and both passwords match
	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2])

		console.log(user)
	return (

		// (user.id !== null) ? 
		//  <Navigate to = "/login" />
		//  :
		<Form onSubmit = {(e) => registerUser(e)}>
		 <h2>Register</h2>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type = 'first-name'
					placeholder = 'Please enter your first name here'
					value = {firstName}
					onChange = {e => setFirstName(e.target.value)}
					required/>	
			</Form.Group>
			<br></br>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type = 'last-name'
					placeholder = 'Please enter your last name here'
					value = {lastName}
					onChange = {e => setLastName(e.target.value)}
					required/>	
			</Form.Group>
			<br></br>
			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type = 'text'
					placeholder = 'Ex. 09148543218'
					maxLength = {11}
					value = {mobileNo}
					onChange = {e => setMobileNo(e.target.value)}
					required/>	
			</Form.Group>
			<br></br>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type = 'email'
					placeholder = 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>	
			</Form.Group>
			<br></br>
			<Form.Group controlId = "password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please input your password'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required/>
			</Form.Group>
			<br></br>
			<Form.Group controlId = "password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please verify your password'
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required/>
			</Form.Group>
			<br></br>


			{ isActive ? // if statement
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
							Register
				</Button>

				: // stands for else 
				<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Register
				</Button>
			}
		</Form>
	)
}