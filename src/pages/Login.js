import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

	const { user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e) {

		e.preventDefault();

		fetch('http://localHost:4000/user/login', {

			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
				console.log(data.access)

				Swal.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})

			} else {
				Swal.fire({
					title: 'Authentication Failed.',
					icon: 'error',
					text: 'Check your login details.'
				})
			}
		})

		// set the user's email to the local storage
		// syntax: localStorage.setItem('propertyName, value')

		// localStorage.setItem('email', email);

		// setUser({
		// 	email:localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('');

		// alert('You are now logged in.')
	}
	  const retrieveUserDetails = (token) => {
        fetch('http://localHost:4000/user/details', {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


	useEffect(() => {
		if(email !== '' && password !== '') {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [email, password])

			console.log(user)

	return (

		(user.id !== null)? 
		 <Navigate to = "/courses" />
		 :
		<Form onSubmit = {(e) => loginUser(e)}>
			<h2>Login</h2>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type = 'email'
					placeholder = 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>	
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please input your password'
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required/>
			</Form.Group>
			<br></br>

			{ isActive ? // if statement
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
							Login
				</Button>

				: // stands for else 
				<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Login
				</Button>
			}
		</Form>

	)
}